package zadanie3;

public class Test3
{
    public static void main(String[] args) {
        String a = "a";
        String b = "b";
        String c = "c";

        String tmp = a;
        a = b;
        b = c;
        c = tmp;
        System.out.println(a + " " + b + " " +  c);
    }
}
