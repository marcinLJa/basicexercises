package zadanie19;

//Zadanie 19*:
//        Napisać program, który pobiera od użytkownika ciąg liczb całkowitych. Pobieranie danych kończone jest podaniem wartości 0 (nie wliczana do danych). W następnej kolejności program powinien wyświetlić sumę największej oraz najmniejszej z podanych liczb oraz ich średnią arytmetyczną.
//        Wskazówka:
//        Nie jest potrzebna tablica.
//        Wskazówka:
//        Czytaj liczby tak długo aż wczytana liczba nie jest 0 (while).

import com.sun.org.apache.xml.internal.resolver.readers.ExtendedXMLCatalogReader;
import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Test19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("podaj ciag liczb calkowitych zakonczonych 0");

        int num = 0;
        int max = 0;
        int min = 0;

        try {
            String token = "";
            if (sc.hasNext()) {
                token = sc.next();
                num = Integer.valueOf(token);
                max = min = num;
            }

            if (num != 0) {
                while (sc.hasNext()) {
                    token = sc.next();
                    num = Integer.valueOf(token);
                    if (num == 0) {
                        break;
                    }
                    if (max < num)
                        max = num;
                    if (min > num)
                        min = num;
                }
                System.out.println("Max: " + max + " Min: " +
                        min + " Avg:" + ((max + min) / 2.0));
            }
        } catch (NumberFormatException e) {
            System.out.println("podales nieliczbe!");
        }

    }

}