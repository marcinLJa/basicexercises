package zadanie8;
/*
Zadanie 8:
        Zadeklaruj trzy zmienne (liczby). Wypisz na ekran wszystkie z nich, a następnie wypisz na ekran największą oraz najmniejszą z nich.
*/


public class Test8 {
    public static void main(String[] args) {
        int a, b , c;

        a = 6;
        b = 7;
        c = 9;
        int max = Max(a, Max(b, c));
        System.out.println("max: " + max);

        int min = Min(a, Min(b, c));
        System.out.println("min: " + min);


    }

    private  static  int Min(int x, int y)
    {
        return (x < y) ? x : y;
    }
    private static int Max(int x, int y)
    {
        return ( x > y) ? x : y;
    }
}
