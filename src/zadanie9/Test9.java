package zadanie9;
//
//Zadanie 9:
//        Posługując się stringami oraz Scannerem. Wykonaj zadanie:
//        zmodyfikuj zadanie z rollercoasterem (zadanie 5). Na początku aplikacji były 3 zmienne.
//        Tym razem nie przypisuj do nich stałych wartości, tylko załaduj je z
//        wejścia od użytkownika.

import java.util.Scanner;

public class Test9 {
    private static final String MSG = "Nie mozesz wejsc do kolejki:";
    private static final int MAX_WAGA = 180;

    private static final int MIN_WZROST = 150;
    private static final int MAX_WZROST = 220;

    private static final int MAX_WIEK = 80;
    private static final int  MIN_WIEK = 10;

    private static Scanner sc;

    private static int printMsgAndGetIntValue(String msg) throws Exception
    {
        System.out.println(msg);
        try {

            String line = sc.nextLine();
            return Integer.valueOf(line);
        }
        catch (NumberFormatException nfe) {
            System.out.println("nieprawidlowy format liczbowy");
            throw nfe;
        }

    }
    public static void main(String[] args) {
        int waga = 100;
        int wzrost = 170;
        int wiek = 9;

        sc = new Scanner(System.in);

        try {
            waga = printMsgAndGetIntValue("podaj wage:");
            wzrost= printMsgAndGetIntValue("podaj wzrost:");
            wiek = printMsgAndGetIntValue("podaj wiek:");
        }
        catch (NumberFormatException e)
        {
            System.out.println("Wychodze z programu");
            return ;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return ;
        }

        if (waga > MAX_WAGA)
        {
            System.out.println(MSG + " przekroczona waga.");
        }
        else if ((wzrost < MIN_WZROST) || (wzrost > MAX_WZROST))
        {
            System.out.println(MSG + " przekroczony wzrost.");
        }
        else if ( (wiek > MAX_WIEK) || (wiek < MIN_WIEK))
        {
            System.out.println(MSG + " przekroczony wiek.");
        }
        else
        {
            System.out.println("Mozesz wejsc na kolejke.");
        }
    }
}
