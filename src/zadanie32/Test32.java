package zadanie32;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Zadanie 32:
//        Napisać program, który sumuje cyfry w tekście podanym przez użytkownika.
//        Przykład:
//        "Ala ma 1 psa i 2 koty. Jola ma 10 rybek i 2 papugi."
//        Wynik:
//        6
public class Test32 {

    public static void main(String[] args) {
        System.out.println("Zliczam sume cyfr w tekscie podanym przez uzytkownika");
        Scanner sc = new Scanner(System.in);


        String s = "Ala ma 1 psa i 2 koty. Jola ma 10 rybek i 2 papugi.";
        System.out.println("Przyklad:");
        System.out.println("\'" + s + "\'");
        System.out.println("suma cyfr: " + SumOfDigits.getSum(s));

        System.out.println("podaj linie:");
        String lan = sc.nextLine();
        System.out.println("suma cyfr: " + SumOfDigits.getSum(lan));
    }
}
class SumOfDigits
{
    public static int getSum(String lan)
    {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(lan);
        int sum = 0;
        while (m.find()) {

            System.out.print(">" + m.group() + "<");
            sum += ( m.group().charAt(0) - (int) '0' );

        }
        return sum;
    }
}