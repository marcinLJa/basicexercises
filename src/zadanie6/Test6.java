package zadanie6;
/*

Napisz aplikację której zadaniem jest liczenie srednich ocen z kilku przedmiotów.
        - ocena_matematyka
        - ocena_chemia
        - ocena_j_polski
        - ocena_j_angielski
        - ocena_wos
        - ocena_informatyka

        Aplikacja ma wyliczac średnią wszystkich ocen, srednią ocen z przedmiotów ścisłych (matematyka, chemia, informatyka), oraz średnią z ocen przedmiotów humanistycznych (pozostałe).

        Wszystkie trzy średnie mają być wypisane na ekran. Zwróć uwagę na zaokrąglenia.

        Jeśli którakolwiek z ocen jest niedostateczna, lub średnia z ocen z którejś części jest niedostateczna, to wyświetl napis:

        Ocena z [część] jest niedostateczna.
*/


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Test6 {

    public static void main(String[] args) {
        int ocena_matematyka = 1;
        int ocena_chemia = 3;
        int ocena_informatyka = 4;
        int ocena_j_polski = 5;
        int ocena_j_angielski = 4;
        int ocena_wos = 4;


        float sredniaScisle = ( (float) ocena_matematyka + ocena_chemia + ocena_informatyka ) / 3;
        float sredniaHumanistyczne = ( (float) ocena_j_polski + ocena_j_angielski + ocena_wos) / 3;
        float srednia = ( sredniaScisle + sredniaHumanistyczne ) /2;

        System.out.println("Srednia scisle: " + String.format("%.2f",sredniaScisle));
        System.out.println("Srednia humanistyczne: " + String.format("%.2f",sredniaHumanistyczne));
        System.out.println("Srednia: " + String.format("%.2f",srednia));

        DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
        otherSymbols.setDecimalSeparator('.');
        otherSymbols.setGroupingSeparator(',');
        DecimalFormat decimalFormat = new DecimalFormat("0.##", otherSymbols);
        //float twoDigitsF = Float.valueOf(decimalFormat.format(f));

        try {
            System.out.println("Srednia scisle: " + decimalFormat.format(sredniaScisle));
            System.out.println("Srednia humanistyczne: "
                    + decimalFormat.format(sredniaHumanistyczne));
            System.out.println("Srednia: "
                    + decimalFormat.format(srednia));
        }
        catch(NumberFormatException e)
        {
            e.getMessage();
            e.printStackTrace();
        }

    }
}