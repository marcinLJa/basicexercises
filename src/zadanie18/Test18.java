package zadanie18;

//Zadanie 18:
//        Napisać program, który pobiera od użytkownika ilość liczb N, a następnie wczytuje
//        N liczb całkowitych od użytkownika.
//        W następnej kolejności program powinien wyświetlić sumę największej oraz najmniejszej z podanych liczb oraz ich średnią arytmetyczną.

import java.util.Arrays;
import java.util.Scanner;

public class Test18 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = 0;


        System.out.println("podaj ilosc liczb");
        try {
            String lan = sc.nextLine();
            number = Integer.valueOf(lan);
            if (number < 0)
            {
                System.out.println("liczba powina  byc nieujemna");
                return;
            }
        }
        catch (NumberFormatException e)
        {
            System.out.println("podales  nie liczbe");
            return;

        }
        System.out.println("podaj " + number + " liczb");
        int tablica[] = new int[number];
        String lan = "";
        int nb = 0;
        int min = 0;
        int max = 0;
        boolean firstLoop = true;
        int count = 0;
        while (count < number) {
            try {
                while(sc.hasNext() && (count < number))
                {
                    lan = sc.next();
                    nb = Integer.valueOf(lan);
                    if (firstLoop) {
                        min = max = nb;
                        firstLoop = false;
                    }
                    else {
                        if (nb < min)
                            min = nb;
                        else if (nb > max)
                            max = nb;
                    }
                    tablica[count] = nb;
                    count++;
                }
            } catch (NumberFormatException e) {
                System.out.println("nie liczba");
                System.out.println("msg: " + e.getMessage());
                System.out.println("cause: " + e.getCause());
            }
        }
        System.out.println("max: " + max + " min " + min + " avg " + (min+max)/2 );
        System.out.println("podales nastepujace liczby:");
        for(int i = 0; i < tablica.length;i++) {
            System.out.println(tablica[i] + " ");
        }
    }
}
