package Zadanie30B;

//Zadanie 30 B*****:
//        Spróbuj odwrócić ciąg stosując rekurencję.

public class Test30B {
    public static void main(String[] args) {
        System.out.println("odwraca ciag znakow");

        StringBuilder ciag = new StringBuilder("abrams");
        StringBuilder odwr = reverseStr(ciag);

    }
    public static StringBuilder reverseStr(StringBuilder str)
    {
        int len =  str.length();
        if (len == 1)
            return str;
        else
        {
            char first = str.charAt(0);
            StringBuilder ending = reverseStr(str.s(1,len));
            return ending.append(first);
        }
    }
}
