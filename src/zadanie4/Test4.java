package zadanie4;

public class Test4 {
    public static void main(String[] args) {
        boolean jest_cieplo = false;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = false;

        // nie jest cieplo lub wieje wiatr
        boolean ubieram_sie_cieplo = (! jest_cieplo) || wieje_wiatr;
        //biore_parasol - jesli nie swieci slonce ale nie wieje wiatr
        boolean biore_parasol = (!swieci_slonce) && (!wieje_wiatr);
        //ubieram_kurtke - jesli wieje, nie ma slonca i nie jest cieplo
        boolean ubieram_kurtke = wieje_wiatr && (! swieci_slonce) && (!jest_cieplo);
    }
}
