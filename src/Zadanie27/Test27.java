package Zadanie27;

import zadanie36.GetInt;

//Zadanie 27:
//        Stwórz program który na wejściu przyjmuje liczbę N a następnie na wyjściu wypisuje tabliczkę mnożenia do tej liczny (tj. do NxN).

public class Test27 {
    public static final int MAX_VALUE = 9;
    public static void main(String[] args) {
        System.out.println("Prezentuje tabliczke mnozenia dla podanej liczby calkowitej <= 12");
        System.out.println();
        int value = GetInt.GetValue("podaj liczne calkowita <= " + MAX_VALUE);
        if (value > MAX_VALUE)
        {
            System.out.println("Liczba wieksza niz " + MAX_VALUE);
        }
        System.out.print("  ");
        for(int y = 1; y <= value; y++)
            System.out.printf(" %2d", y);
        System.out.println();
        for(int x = 1; x <= value; x++)
        {
            System.out.printf("%2d ", x);
            for(int y = 1; y <= value; y++)
            {
                System.out.printf("%2d ", x * y);
            }
            System.out.println();
        }

    }
}
