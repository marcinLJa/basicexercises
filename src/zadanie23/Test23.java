package zadanie23;


//Zadanie 23*: (bardzo matematyczne)
//        Napisać program, który sprawdza, czy podana liczba całkowita n,n > 1, jest liczbą pierwszą.

import java.util.Scanner;

public class Test23 {
    public static void main(String[] args) {
        System.out.println("Sprawdzam czy podana liczba jest pierwsza");
        System.out.println("podaj liczbe calkowita n, n > 1");
        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();

        System.out.println("Liczba " + liczba +
                (Pierwsza.czyPiersza(liczba) ? " " : " nie " )+
                "jest liczba pierwsza");

//        for (int i = 2; i < 1025; i++)
//        {
//            if (Pierwsza.czyPiersza(i)) {
//                System.out.println("Liczba " + i +
//                        (Pierwsza.czyPiersza(i) ? " " : " nie ") +
//                        "jest liczba pierwsza");
//            }
//
//
//        }


    }
}

class Pierwsza
{
    public static boolean czyPiersza(int liczba)
    {
        int tableSize = liczba + 1;
        boolean pierwsza[] = new boolean[tableSize];

        for(int  i = 0; i < tableSize; i++)
            pierwsza[i] = true;

        //sito arystotelesa
        int i = 2;
        while( i < tableSize)
        {
            if (i == tableSize -1 )
                break;

            // zakesl wszystkie wielokrotnosci
            for(int wielokr = i; wielokr < tableSize; wielokr += i)
            {
                pierwsza[wielokr] = false;
            }
            if (pierwsza[liczba] == false)
            {
                break;
            }
            // znaj nastepna wartosc ktorej wielokrotnosci nalezy sprawdzac
            for(int x = i; x < tableSize; x++)
            {
                if (pierwsza[x] == true) {
                    i = x;
                    break;
                }
            }

        }
        return (pierwsza[liczba]);
    }
}