package zadanie24;

//Zadanie 24:
//        Napisać program, który:
//        • utworzy tablicę 10 liczb całkowitych i wypełni ją wartościami losowymi z
//        przedziału [−10, . . . , 10],
//        • wypisze na ekranie zawartość tablicy,
//        • wyznaczy najmniejszy oraz najwięszy element w tablicy,
//        • wyznaczy średnią arytmetyczną elementów tablicy,
//        • wyznaczy ile elementów jest mniejszych, ile większych od średniej.
//        • wypisze na ekranie zawartość tablicy w odwrotnej kolejności, tj. od ostatniego
//        do pierwszego.
//
//        Wszystkie wyznaczone wartości powinny zostać wyświetlone na ekranie.
//        Wylosowane liczby:
//        -3 9 2 -10 -3 -4 -1 -5 -10 8
//        Min: -10, max: 9
//        Średnia: -1,00
//        Mniejszych od śr.: 6
//        Większych od śr.: 3
//        Liczby w odwrotnej kolejności:
//        8 -10 -5 -1 -4 -3 -10 2 9 -3

import java.util.Random;

public class Test24 {
    private static final int ROZMIAR = 10;
    public static void main(String[] args) {
        int table[] = new int[ROZMIAR];

        System.out.println("operacje na tablicy int'ow");

        Random gen = new Random();

        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        double avg = 0;
        for(int i = 0; i < ROZMIAR; i++)
        {
            // liczby z zakresu -10 do 10
            table[i] = gen.nextInt(21) - 10;
            if (min > table[i])
                min = table[i];
            if (max < table[i])
                max = table[i];
            avg += table[i];
        }
        System.out.println("wylosowane liczby:");
        for(int el : table)
        {
            System.out.print(el + " ");
        }
        System.out.println();;

        System.out.println("Min: " + min);
        System.out.println("Max: " + max);
        System.out.println("Avg " +  avg/10.0);
        int gTAvg = 0;
        int lTAvg = 0;
        for(int i = 0; i < ROZMIAR; i++)
        {
            if (table[i] > avg)
                gTAvg++;
            else if (table[i] < avg)
                lTAvg++;

        }
        System.out.println("Wiekszych  od sredniej:" + gTAvg);
        System.out.println("Mniejszych od sredniej:" + lTAvg);

    }
}
