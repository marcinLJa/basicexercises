package zadanie13;

import com.sun.org.apache.regexp.internal.RE;
import zadanie12.ReadPositiveBoundary;

//Zadanie 13:
//        Napisać program pobierający od użytkownika dwie liczby całkowite A oraz B, A < B, a następnie wyznaczający sumę ciągu liczb od A do B, czyli sumę ciągu (A,A + 1,...,B). Obliczenia należy wykonać dwukrotnie stosując kolejno pętle: while, for.
//        Przykład: Dla A = 4 i B = 11 program powinien wyświetlić: 60 60

public class Test13 {
    public static void main(String[] args) {
        ReadPositiveBoundary bounds = new ReadPositiveBoundary();

        bounds.ReadFromConsole();

        int upper = bounds.getUpper();
        int lower = bounds.getLower();

        int sum = 0;
        for(int iter = lower; iter < upper; ++iter) {
            sum += iter;
        }
        System.out.println("Suma od " + lower + " do " + upper + " to " + sum);
        /*sum = 0;
        int iter = lower;
        while(iter <= upper)
        {

            sum += iter;

        }
        System.out.println("Suma od " + lower + " do" + upper + "to " + sum);*/
    }


}
