package zadanie33;

//Zadanie 33:
//        Napisz program, który sprawdzi, czy w podanym przez użytkownika wyrażeniu
//        arytmetycznym nawiasy są poprawnie sparowane. Wyrażenie podawane jest jako
//        pojedynczy łańcuch znaków. Program powinien wyświetlić stosowny komunikat.
//        Przykład a:
//        "2 * (3.4 - (-7)/2)*(a-2)/(b-1)))"
//        Wynik:
//        Błędne sparowanie nawiasów
//        Przykład b:
//        "2 * (3.4 - (-7)/2)*(a-2)/(b-1))"
//        Wynik:
//        OK

//(2) * ((3.4) - ((-7)/2)))
public class Test33 {
    public static void main(String[] args) {
        System.out.println("Sprawdzam poprawne sparowanie nawiasow.");

        String test1  = "(2) * ((3.4) - ((-7)/2))*(a-2)/(b-1)";

        String test2  = "2 * (3.4 - (-7)/2)*(a-2)/(b-1))";

        Nawiasy test = new Nawiasy();

        test.setTested(test1);
        System.out.println("lancuch " +
                ">" + test1 + "<"
                + ((test.testNawiasow()) ? " poprawne" : "niepoprawne"));

        test.setTested(test2);
        System.out.println("lancuch " +
                ">" + test2 + "<"
                + ((test.testNawiasow()) ? " poprawne" : "niepoprawne"));
    }
}
class Nawiasy {
    String parsed;
    String tested;
    int stos = 0;

    public boolean testNawiasow() {
        for(int i=0; i < tested.length(); i++)
        {
            if (tested.charAt(i) == '(')
                ++stos;
            if (tested.charAt(i) == ')') {
                if (stos > 0)
                    --stos;
                else {
                    parsed = tested.substring(0, i + 1);
                    System.out.println("problem w >" + parsed + "<");
                    return false;
                }
            }
        }
        return true;
    }

    public String getParsed() {
        return parsed;
    }

    public void setTested(String tested) {
        this.tested = tested;
    }
}