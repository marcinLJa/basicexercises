package Zadanie26;

//Zadanie 26*****:
//        Napisać program, który wczytuje od użytkownika liczbę całkowitą, a następnie
//        wyświetla jej reprezentację w kodzie binarnym (ZM). Podczas konwersji liczby
//        należy kolejne jej bity zapisywać w pomocniczej tablicy liczb całk. o rozmiarze
//        32. Konwersji należy dokonać korzystając z operacji dzielenia całkowitego oraz
//        operacji modulo.
//
//        Przykład:
//        Wejście:
//        -75 (liczba podana przez użytkownika)
//        Wynik:
//        Liczba -75 binarnie: 1.1001011

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Test26 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("konwertuje liczbe calkowita dziesiatna na postac binarna ze znakiem");
        System.out.println("podaj liczbe:");
        int liczba = sc.nextInt();

        System.out.println("Liczba " + liczba +
                "ma reprezetacje binarna " + convertToBin.getBinary(liczba));

    }
}
class convertToBin
{

    public static String getBinary(int value)
    {
        String str = "";
        String lead = "";
        if (value < 0)
            lead = "1.";
        value = Math.abs(value);
        while(value > 0)
        {
            str = ( (value%2 != 0) ? "1" : "0") + str;
            value /= 2;
        }
        return lead + str;
    }
}
