package zadanie12;
//
//Zadanie 12:
//        Napisać program, który pobiera od użytkownika liczbę całkowitą dodatnią, a następnie wyświetla na ekranie kolejno wszystkie liczby niepatrzyste nie większe od podanej liczby. Przykład, dla 15 program powinien wyświetlić 1, 3, 5, 7, 9, 11, 13, 15.
//        B)
//        Napisać program, który wykorzystuje pętle i wypisuje liczby podzielne przez 3 lub przez 5 w przedziale od 3 do 100 (3,5,6,9,10,...)
//        C)
//        Napisać program, który wykorzystuje pętle i wczytuje od użytkownika przedział liczbowy (użytkownik ma podać dolną granicę zakresu i górną) (początek i koniec przedziału), a następnie wypisz wszystkie liczby z tego przedziału podzielne przez 6.
//

import java.util.Scanner;

public class Test12 {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int liczba = -1;
        String line;
        do {
            System.out.println("Podaj liczbe calkowita nieujemna:");

            try {
                line = sc.nextLine();
                liczba = Integer.valueOf(line);
                if (liczba < 0) {
                    System.out.println("Podana liczba jest ujemna.");
                }
            } catch (NumberFormatException e) {
                System.out.println();
                System.out.println("Nie prawidlowa liczba!");
            }

        }
        while (liczba < 0);

        System.out.println("podana liczba " + liczba);
        for (int i = 0; i < liczba; i++) {
            if ((i % 2) != 0)
                System.out.printf("%d ", i);
        }
        System.out.println();

        System.out.println("Liczba podzielne przez 3 i 5 z zakresu od 3 do 100");
        for(int value = 3; value <= 100; ++value)
        {
            if ((value % 3) == 0)
            {
                System.out.println(value + " ");
            }
            else if ((value % 5) == 0)
            {
                System.out.println(value + " ");
            }
        }
        Scanner sc2 = new Scanner(System.in);
        int lower = 0;
        int upper = 0;

        do
        {
            try
            {
                System.out.println("podaj dolny limit dodatni calkowity");
                String line1 = sc2.nextLine();
                lower = Integer.valueOf(line1);
                if (lower < 0)
                {
                    System.out.println("podaj liczbe dodatnia");
                }
                else {
                    System.out.println("podaj gorny limit dodatni calkowity");
                    line1 = sc2.nextLine();
                    upper = Integer.valueOf(line1);
                    if ((upper < 0) || (upper < lower)) {
                        System.out.println("gorny zakres musi byc dodatnia i niemniejsza od dolnego zakresu");
                    } else {
                        break;
                    }
                }
            }
            catch( NumberFormatException e) {
                System.out.println("musi byc liczba dodatnia calkowita");
            }
        }
        while (true);
        System.out.println("podany zakres: " + lower + " " + upper);
        System.out.println("liczby podzielne przez 6 z podanego zakresu:");
        StringBuffer liczbyZZakresu = new StringBuffer(20);

        for(int value = lower; value < upper; value++)
        {
            if ((value % 6) == 0) {
                liczbyZZakresu.append(value);
                liczbyZZakresu.append(' ');
            }
        }
        System.out.println(liczbyZZakresu);
    }
}
