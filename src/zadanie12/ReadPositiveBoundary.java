package zadanie12;


import java.util.Scanner;



public class ReadPositiveBoundary
{
    private int lower;
    private int upper;


    public ReadPositiveBoundary() {
        this.lower = 0;
        this.upper = 0;
    }


    public void ReadFromConsole()
    {
        Scanner sc2 = new Scanner(System.in);

        do
        {
            try
            {
                System.out.println("podaj dolny limit dodatni calkowity");
                String line1 = sc2.nextLine();
                lower = Integer.valueOf(line1);
                if (lower < 0)
                {
                    System.out.println("podaj liczbe dodatnia");
                }
                else {
                    System.out.println("podaj gorny limit dodatni calkowity");
                    line1 = sc2.nextLine();
                    upper = Integer.valueOf(line1);
                    if ((upper < 0) || (upper < lower)) {
                        System.out.println("gorny zakres musi byc " +
                                "dodatnia i niemniejsza od dolnego zakresu");
                    } else {
                        break;
                    }
                }
            }
            catch( NumberFormatException e) {
                System.out.println("musi byc liczba dodatnia calkowita");
            }
        }
        while (true);
    }
    public int getLower() {
        return lower;
    }

    public int getUpper() {
        return upper;
    }
}
