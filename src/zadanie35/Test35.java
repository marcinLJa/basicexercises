package zadanie35;

//Zadanie 35:
//        1.  Napisz program, który prosi użytkownika o dwie liczby a i b, i wyświetla wynik dzielenia a/b. Jeżeli b jest ujemne program powinien wyświetlić odpowiedni komunikat.
//        a.  Wariant 1: Zastosuj instrukcję if
//        b.  Wariant 2: Zastosuj instrukcję try-catch

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test35 {
    public static void main(String[] args) {
        System.out.println("Wykonuje dzielenie calkowite, podaj dwie liczb");
        Scanner sc = new Scanner(System.in);
        int a = -1;
        int b = -1;
        while (true) {
            try {


                if (a < 0) {
                    System.out.println("Podaj dzielna");
                    a = sc.nextInt();
                    if (a < 0) {
                        NumberFormatException e1 = new NumberFormatException("Ujemna dzielna");
                        throw e1;
                    }
                }

                System.out.println("Podaj dzielnik");
                b = sc.nextInt();
                if (b < 0) {
                    NumberFormatException e1 = new NumberFormatException("Ujemny dzielnik");
                    throw e1;
                }
                else
                {
                    break;
                }
            } catch (NumberFormatException e) {
                System.err.println(e.getMessage());
            }
            catch (InputMismatchException e)
            {
                System.err.println("nie podales liczby calkowitej");
                System.out.println(sc.nextLine());
            }
        }
        System.out.println("wynik dzielenia:" + a/b);

    }
}
