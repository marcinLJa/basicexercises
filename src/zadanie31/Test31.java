package zadanie31;

//Zadanie 31:
//        Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie sprawdza, czy podany ciąg jest palindromem.

import java.util.Scanner;

public class Test31 {
    public static void main(String[] args) {
        System.out.println("Sprawdzam czy ciag znakow jest palindronem");

        Scanner sc = new Scanner(System.in);

        String lancuch = sc.next();


        boolean palindron = true;
        for(int i = 0; i < (1 + lancuch.length()/2); i++)
        {
            if (lancuch.charAt(i) != lancuch.charAt(lancuch.length() - 1 - i))
                palindron = false;
        }
        System.out.println("Podany lancuch " + lancuch + " " +
                ((palindron) ? " " : "nie " ) + "jest palidronem." );
    }
}
