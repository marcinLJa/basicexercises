package Zadanie34;


//Zadanie 34:
//        Napisz program, który umożliwia szyfrowanie podanego ciągu znaków przy użyciu
//        szyfru Cezara, który jest szczególnym przypadkiem szyfru podstawieniowego
//        monoalfabetycznego.
//        Użytkownik program podaje tekst do zaszyfrowania oraz liczbę n, o którą przesunięty
//        jest alfabet za pomocą którego szyfrujemy tekst. Dla uproszczenia można
//        przyjąć, że łańuch wejściowy składa się tylko z małych liter alfabetu angielskiego,
//        tj. ’a’ – ’z’ (26 znaków) oraz spacji.
//        Przykład 1.
//        Podaj łańcuch znaków do zaszyfrowania: abrakadabraz
//        Podaj przesunięcie: 2
//        Zaszyfrowany tekst: cdtcmcfcdtcb
//        Przykład 2.
//        Podaj łańcuch znaków do zaszyfrowania: cdtcmcfcdtcb
//        Podaj przesunięcie: -2
//        Zaszyfrowany tekst: abrakadabraz

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Scanner;

public class Test34 {
    public static void main(String[] args) {
        System.out.println("Szyfr cezara - przsuniecie o zadana liczbe znakow");


        System.out.println("Podaj tekst:");
        Scanner sc = new Scanner(System.in);
        String lan = sc.nextLine();

        int maxShift = (int) 'z' - (int) 'a' + 1;
        int shift = 0;
        System.out.println("podaj przesuniecie " +
                "(liczba calkowita od " + (-maxShift) + " do " + maxShift);
        try {
            shift = sc.nextInt();
            if ((shift > maxShift) || (shift < -maxShift))
            {
                shift = shift % maxShift;
                System.out.println("Przyjmuje wartosc " + shift);
            }
        }
        catch (NumberFormatException e)
        {
            System.out.println("podales nie liczbe");
        }


        System.out.println(SzyfrCezara.szufruj(lan, shift));
    }
}
class SzyfrCezara {
    static private final int MAX_SHIFT = ((int) 'z' - (int) 'a' + 1);
    public static String szufruj(String lan, int shift)
    {
        StringBuffer outString = new StringBuffer("");
        for( int i = 0; i < lan.length(); i++)
        {
            char zn = lan.charAt(i);
            if (Character.isLowerCase(zn) && Character.isLetter(zn)) {
                zn = (char) ('a' + ( (((zn - 'a') + (char)shift) %  MAX_SHIFT)));
            }
            outString.append(zn);
        }
        return outString.toString();
    }
}