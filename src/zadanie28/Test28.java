package zadanie28;

//Zadanie 28*:
//        Napisz program, który pobiera od użytkownika dodatnią liczbę naturalną n i
//        tworzy tablicę a zmiennych typu logicznego (boolean) o rozmiarze n×n. Następnie
//        program powinien wypełnić utworzoną tablicę, tak by a[i][j] = true jeżeli liczby
//        (i+1) oraz (j+1) są względnie pierwsze, tzn. nie mają wspólnych dzielników poza
//        1. Tak utworzoną tablicę należy wypisać na ekranie, przy czym dla wartości true
//        należy wyświetlić znak ”+”, natomiast dla wartości false znak ”.”. Przykład:
//        Podaj liczbę (> 0): 10
//          1 2 3 4 5 6 7 8 9 10
//        1 + + + + + + + + + +
//        2 + . + . + . + . + .
//        3 + + . + + . + + . +
//        4 + . + . + . + . + .
//        5 + + + + . + + + + .
//        6 + . . . + . + . . .
//        7 + + + + + + . + + +
//        8 + . + . + . + . + .
//        9 + + . + + . + + . +
//        10 + . + . . . + . + .

import java.util.Scanner;

public class Test28 {
    public static void main(String[] args) {
        System.out.println("Tworze tablice liczb wzglednie pierwszych dla podanej wartosc");

        Scanner sc = new Scanner(System.in);

        int rozmiar = 4;

        //rozmiar = sc.nextInt();

        boolean table[][] = new boolean[rozmiar][rozmiar];

        for( int x = 0; x < rozmiar ; x++)
        {
            for (int y =0; y < rozmiar ; y++)
            {
                if (pierwsze.wzgledniePierwsze(x + 1, y + 1))
                    table[x][y] = true;

                else
                    table[x][y] = false;
            }
        }
        pierwsze.drukuj(table);

    }
}
class pierwsze {

    public static boolean wzgledniePierwsze(int x, int y)
    {

        for(int i = 2; i < Math.min(x,y); i++)
            if (!(((x % i) == 0) && ((y % i) == 0)))
                return false;
        return true;
    }
    public static void drukuj(boolean [][] table)
    {
        int rozmiar = table.length;

        System.out.print(" ");
        for (int y =1; y <= rozmiar ; y++)
            System.out.print(" " + y);
        System.out.println();
        for( int x = 0; x < rozmiar ; x++)
        {
            if (x >= 9)
                System.out.println(x+1);
            else
                System.out.print(x+1 + " ");

            for (int y =0; y < rozmiar ; y++)
            {

                if (table[x][y])
                    System.out.print("+ ");

                else
                    System.out.print(". ");
            }
            System.out.println();
        }
        System.out.println();
    }
}