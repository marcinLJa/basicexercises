package zadanie30A;


//Zadanie 30 A:
//        Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie tworzy
//        łańcuch będący odwróceniem podanego łańcucha i wyświetla go na ekranie.
//        Przykładowo, dla łańcucha „Kot” wynikiem powinien być łańcuch „toK”.

import java.util.Scanner;

public class Test30A {
    public static void main(String[] args) {
        System.out.println("Odwracam lancuch znakow tj dla \"Kot\" wynikiem jest \"toK\"");

        Scanner sc = new Scanner(System.in);

        String lancuch = sc.next();
        String res = "";
        for(int i = lancuch.length() -1; i >=0; i--)
        {
            res += lancuch.charAt(i);
        }
        System.out.println(" " + res);
    }
}
