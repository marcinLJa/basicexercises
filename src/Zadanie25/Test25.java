package Zadanie25;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.Arrays;
import java.util.Random;
import java.util.Vector;

//Zadanie 25:
//        Napisać program, który utworzy tablicę 20 liczb całkowitych z przedziału 1 . . . 10,
//        a następnie wypisze na ekranie ile razy każda z liczb z tego przedziału powtarza
//        się w tablicy.
//        Przykład:
//        Wylosowane liczby: 6 5 4 5 10 5 8 3 10 6 6 6 4 3 2 8 1 3 4 7
//        Wystąpienia:
//        1 - 1
//        2 - 1
//        3 - 3
//        4 - 3
//        5 - 3
//        6 - 4
//        7 - 1
//        8 - 2
//        9 - 0
//        10 - 2
public class Test25 {
    private static final int SIZE = 20;
    public static void main(String[] args) {
        System.out.println("Tworze tablice 20 losowych liczb calowitych z przedzialu od 1 do 10 i wyliczam ilosc wystapien kazdej z liczb");

        Integer[] table = new Integer[SIZE];
        Random gen = new Random();
        int max = 10;
        int min = 1;
        // wylicza ilosc wystapien
        int[] table2 = new int[max];
        Arrays.fill(table2, 0);
        for(int e = 0; e < table.length; e++)
        {

            //generate from <0, bound)
             table[e] = min + gen.nextInt(max - min + 1);
             //ilosc wystapien
             ++(table2[table[e]-1]);
        }

        System.out.println("tablica: " + Arrays.deepToString(table));
        for(int e = 0; e < table2.length; ++e )
        {
            System.out.println("el " +( e + 1) + " wystepuje " + table2[e]);
        }





    }
}
