package zadanie21;


//Zadanie 21*****:
//        Napisać program rysujący w konsoli „choinkę” złożoną ze znaków gwiazdki (*).
//        Użytkownik programu powinien podać liczbę całkowitą n, n > 0, określającą
//        wysokość choinki (liczbę wierszy).
//        Przykład: dla n = 5 wynik powinien wyglądać następująco:
//        *
//        ***
//        *****
//        *******
//        *********

import java.util.Scanner;

public class Test21 {
    public static void main(String[] args) {
        int liczbaPoziomow = 0;

        System.out.println("Rysuje choinke n-poziomowa");
        System.out.println("podaj liczbe poziomow:");
        Scanner sc = new Scanner(System.in);
        liczbaPoziomow = sc.nextInt();

        int liczbaSpacji = liczbaPoziomow - 1;
        int liczbGwiazdek  = 0;
        for(int poziom = 1; poziom <= liczbaPoziomow; poziom++)
        {
            // 1   *    - 2sp * 2sp
            // 2  ***    1sp *** 1sp
            // 3 *****      *****
            for (int i = 0; i < liczbaSpacji; i++)
               System.out.print(" ");

            for (int i = 0; i < liczbGwiazdek; i++)
                System.out.print("*");

            System.out.print("*");


            for (int i = 0; i < liczbGwiazdek; i++)
                System.out.print("*");


            System.out.println();

            liczbaSpacji--;
            liczbGwiazdek++;

        }



    }
}
