package zadanie7;

/*
Zadanie 7:
        Napisać program służący do konwersji wartości temperatury podanej w stopniach
        Celsjusza na stopnie w skali Fahrenheita (stopnie Fahrenheita = 1.8 * stopnie
        Celsjusza + 32.0)
*/

public class Test7 {
    private static double CelcjuszDoF(double stopnieC)
    {
        return ( stopnieC * 1.8 ) + 32;
    }
    public static void main(String[] args) {
        double stopnieF = 0;
        int interacje = 0;

        System.out.println("Przykladowa konwersja stopni Celcjusza na Faranheit\'a:");
        for( double stopnieC = 10.01, iteracje = 0; iteracje < 10; stopnieC += 10, ++iteracje)
        {
            System.out.println("C=" + String.format("%.3f", stopnieC)
                    + " F=" + String.format("%.3f", CelcjuszDoF(stopnieC)));
            System.out.printf("new C=%3.2f F=%3.4f\n", stopnieC, CelcjuszDoF(stopnieC));
        }



    }
}
