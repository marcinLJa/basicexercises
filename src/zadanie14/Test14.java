package zadanie14;

//Zadanie 14*:
//        Napisz program który wypisuje litery od 'a' do 'z';
public class Test14 {
    public static void main(String[] args) {
        for(char zn = 'a'; zn <= 'z'; ++zn)
        {
            System.out.printf("%c ", zn);
        }
    }
}
