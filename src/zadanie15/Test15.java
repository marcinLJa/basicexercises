package zadanie15;

import java.util.Scanner;

//Zadanie 15:
//        Napisz program, który wypisuje “Hello World” zadaną przez użytkownika ilość razy.
//
//Zadanie 16:
//        Napisz program, który wypisuje “Hello World” dopóki użytkownik podaje liczby większe od 0.
public class Test15 {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int liczba = -1;
        try
        {
            System.out.println("podaj liczba calkowita");
            liczba = sc.nextInt();
        }
        catch (Exception e)
        {
            System.out.println("to nie jest liczba");
            return;
        }
        if (liczba < 0)
        {
            System.out.println("podales liczbe mniejsza od 0");
        }

        for(int x = 0; x < liczba; x++)
        {
            System.out.println("Hello World");
        }


        System.out.println("bede wyswietla hello dopuki bedziesz podawal liczby dodatnia");

        {
            sc = new Scanner(System.in);
            liczba = -1;

            do {

                System.out.println("hello world");
                try {

                    liczba = sc.nextInt();
                    if (liczba <= 0) {
                        return;
                    }
                } catch (Exception e) {
                    System.out.println("to nie jest liczba");
                    return;
                }
            } while (true);
        }
    }
}
