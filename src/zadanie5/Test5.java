package zadanie5;

public class Test5 {
    private static final String MSG = "Nie mozesz wejsc do kolejki:";
    private static final int MAX_WAGA = 180;

    private static final int MIN_WZROST = 150;
    private static final int MAX_WZROST = 220;

    private static final int MAX_WIEK = 80;
    private static final int  MIN_WIEK = 10;

    private static boolean sprawdzWage(int waga)
    {
        if (waga > MAX_WAGA)
            return false;
        else
            return true;
    }
    private static boolean sprawdzWiek(int wiek)
    {
        return true;
    }
    public static void main(String[] args) {


//      /*  Zadanie 5: Zadeklaruj dwie zmienne - 'waga' oraz 'wzrost'. Przypisz do nich jakieś //wartości. Stwórz instrukcję warunkową ('if') który sprawdza czy osoba (np. taka która wchodzi
//     na kolejkę/rollercoaster) jest wyższa niż 150 cm wzrostu i nie przekracza wagą 180 kg.
        int waga = 100;
        int wzrost = 170;
        int wiek = 9;


        if (waga > MAX_WAGA)
        {
            System.out.println(MSG + " przekroczona waga.");
        }
        else if ((wzrost < MIN_WZROST) || (wzrost > MAX_WZROST))
        {
            System.out.println(MSG + " przekroczony wzrost.");
        }
        else if ( (wiek > MAX_WIEK) || (wiek < MIN_WIEK))
        {
            System.out.println(MSG + " przekroczony wiek.");
        }
        else
        {
            System.out.println("Mozesz wejsc na kolejke.");
        }

////                Zadanie 5
////        a: Dopisz do poprzedniej aplikacji dodatkową zmienną - wiek. Jeśli osoba jest młodsza //niż 10 lat, lub starsza niż 80, to nie może wejść na kolejkę.
//
//                b: Dopisz/zmień do/w poprzedniej aplikacji - osoba może wejść na kolejkę jeśli jej wzrost jest od 150 do 220 cm wzrostu.
//
//        c: Dopisz deskryptywne wyjasnienia. Jesli osoba nie moze wejsc na kolejke, to wypisz na konsole odpowiedni komunikat dlaczego. np.
//
//                Jesli osoba nie moze wejsc z powodu wagi, to powinien sie wypisac komunikat ze nie moze wejsc bo przekracza limit wagowy
//
//        Jesli osoba nie moze wejsc z powodu wieku, to powinien sie wypisac INNY komunikat o tym ze nie moze wejsc z powodu wieku.*/
    }
}
