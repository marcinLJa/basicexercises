package zadanie36;


import java.util.InputMismatchException;
import java.util.Scanner;

public class GetInt
{
    public static int GetValue(String msg) {
        Scanner sc = new Scanner(System.in);
        int a = -1;
        while (true) {
            try {
                System.out.println(msg);
                a = sc.nextInt();
                break;
            } catch (InputMismatchException e) {
                System.err.println("nie podales liczby calkowitej.");
                System.out.println(sc.nextLine());
            }
        }
        return a;
    }
}
