package zadanie36;

//Zadanie 36:
//        Stwórz program obliczający równanie kwadratowe (http://matematyka.pisz.pl/strona/54.html). Na wejściu użytkownik podaje wartość zmiennych A, B i C,
//        a na wyjściu wypisują sie odpowiednie komunikaty o wartościach x1 i x2 lub x0, a także wartość delty.

import java.util.InputMismatchException;
import java.util.Scanner;

public class Test36 {
    public static void main(String[] args) {
        System.out.println("Wyliczam pierwiastki oraz delte rownania kwadratowego postaci Ax2 + Bx + C = 0");
        double a = (double) GetInt.GetValue("Podaj A:");
        double b = (double) GetInt.GetValue("Podaj B:");
        double c = (double) GetInt.GetValue("Podaj C:");

        double delta = b * b - 4 * a * c;

        System.out.println("delta " + delta);
        if (delta < 0)
        {
            System.out.println("Brak pierwiastow rownania");
        }
        else {

            double x1 = (-b - Math.pow(delta, 0.5)) / (2 * a);
            if (delta == 0) {
                System.out.println("Jedyny pierwiastek x0 " + x1);
            }
            if (delta > 0){
                double x2 = (-b + Math.pow(delta, 0.5)) / (2 * a);
                System.out.println("pierwiastek x1 " + x1);
                System.out.println("pierwiastek x2 " + x2);

            }
        }
    }
}
