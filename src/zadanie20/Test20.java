package zadanie20;

//Zadanie 20:
//        Gra w ”Za dużo, za mało”. Komputer losuje liczbę z zakresu 1 . . . 100, a gracz
//        (użytkownik) ma za zadanie odgadnąć, co to za liczba poprzez podawanie kolejnych
//        wartości. Jeżeli podana wartość jest:
//        • większa – wyświetlany jest komunikat „Podałeś za dużą wartość”,
//        • mniejsza – wyświetlany jest komunikat „Podałeś za małą wartość”,
//        • identyczna z wylosowaną – wyświetlany jest komunikat „Gratulacje” i gra
//        się kończy.

import java.util.Random;
import java.util.Scanner;



public class Test20 {

    public static void main(String[] args) {
        int number; // user guessed number
        int genNum;

        System.out.println("gra w zgadnij liczbe.");

        generateRandomNumber rand = new generateRandomNumber(1, 100);

        genNum = rand.getGenNum();

        System.out.println("Podaj liczbe od 1 do 100");

        while (true) {
            try {
                number = getNumberFromCosole();
                if ((number < 1) || (number > 100)) {
                    System.out.println("Liczba nie jest od 1 do 100. Wychodze");
                    return;
                }
                if (number == genNum) {
                    System.out.println("zgadles!!");
                    break;
                }
                else
                {

                    System.out.println("nie zgadles, podaj liczbe " +
                            ((number > genNum) ? "mniejsza" : "wieksza"));

                }
            } catch (NumberFormatException e) {
                System.out.println("podales nieliczbe. Wychodze.");
                return;
            }
        }
    }


    private static int getNumberFromCosole() throws NumberFormatException
    {
        Scanner sc = new Scanner(System.in);

        String line = sc.nextLine();

        return (int) Integer.valueOf(line);
    }

}

class generateRandomNumber {
    private int genNum; // generated number

    // generate number from <min, max>
    public generateRandomNumber(int min, int max) {
        Random gen = new Random();
        //generate from <0, bound)
        genNum = min + gen.nextInt(max - min + 1);
    }

    public int getGenNum() {
        return genNum;
    }
}
