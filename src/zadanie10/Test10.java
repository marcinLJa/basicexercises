package zadanie10;

//Zadanie 10:
//        Napisać program obliczający należny podatek dochodowy od osób ﬁzycznych. Program ma pobierać od użytkownika dochód i po obliczeniu wypisywać na ekranie należny podatek. Podatek obliczany jest wg. następujących reguł:
//        • do 85.528 podatek wynosi 18% podstawy minus 556,02 PLN,
//        • od 85.528 podatek wynosi 14.839,02 zł + 32% nadwyżki ponad 85.528,00

import java.util.Arrays;
import java.util.List;

public class Test10 {
    static float ODLICZENIE = 552;

    public static void main(String[] args) {
        float podatek = 0;
        float dochod = 0;
        float podatekProcentowy[] = { 0.18f, 0.30f };


        dochod = 10000;

        if (dochod <= 85528)
        {
            podatek = dochod * podatekProcentowy[0];
            if (podatek >= ODLICZENIE)
                podatek -= ODLICZENIE;
        }
        else
        {
            podatek = ((dochod - 85528) * podatekProcentowy[1]) + 14839.02f;
        }
        System.out.println("Podatek pochodowy");
        System.out.printf("dochod: %.02f podatek: %.02f\n", dochod, podatek);
    }
}