package zadanie29;


//Zadanie 29:
//        Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie wy-
//        świetla informację o tym ile razy w tym ciągu powtarza się jego ostatni znak.
//        Przykład, dla ciągu „Abrakadabra” program powinien wyświetlić 4, ponieważ
//        ostatnim znakiem jest literka „a”, która występuje w podanym ciągu łącznie 4
//        razyh

import java.util.*;

public class Test29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Map<Character, Integer> wystapienia  = new HashMap<>();

        System.out.println("podaj lancuch w ktorym policzyc wystapienia znakow");
        String line = sc.nextLine();

        char tab[] = line.toCharArray();




        for(int i = 0; i < tab.length; i++)
        {
            Integer count = wystapienia.get(tab[i]);
            if (count != null)
            {
                wystapienia.replace(tab[i], ++count);
            }
            else
            {
                    wystapienia.put(tab[i], 1);
            }

        }
        wystapienia = new TreeMap<>(wystapienia);
        System.out.println(wystapienia);
        System.out.println("wystapienia liter w lancuchu ");
        for (HashMap.Entry<Character, Integer> entry : wystapienia.entrySet()) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }
        char zn = tab[tab.length - 1];
        System.out.println("Ostatni znak " + "\'" + zn + "\'" + " wystepuje " + (wystapienia.get(zn)) );

    }

    public static  List<Character> toList(char[] objects){
        List<Character> lista = new LinkedList<>();
        for (char object: objects) {
            lista.add(object);
        }
        return lista;
    }

}
