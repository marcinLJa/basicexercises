package zadanie22;

import java.util.Arrays;
import java.util.Scanner;

//Zadanie 22:
//        Napisać program, dla podanej liczby całkowitej wyświetla jej dzielniki. Przykładowo, dla liczby 21 dzielniki to: 1, 3, 7, 21.
public class Test22 {
    public static void main(String[] args) {
        System.out.println("znajduje dzielniki podanej liczby" );
        System.out.println("podaj liczbe calkowita dodatnia");

        Scanner sc = new Scanner(System.in);
        int liczba = sc.nextInt();

        int pierw = (int) Math.pow((double) liczba, 0.5);
        int tablica[] = new int[pierw * 2];
        int idx = 0;
        for(int i = 1; i <= pierw; i++ )
        {
            if ((liczba % i) == 0) {
                tablica[idx++] = i;
                tablica[idx++] = liczba / i;
            }
        }
        int sorted[] = Arrays.copyOfRange(tablica, 0, idx);
        Arrays.sort(sorted);
        System.out.println(Arrays.toString(sorted));

    }
}
