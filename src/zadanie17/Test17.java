package zadanie17;


//Zadanie 17:
//        Napisać program, który wczytuje od użytkownika liczbę całkowitą dodatnią n, a następnie wyświetla na ekranie wszystkie potęgi liczby 2 nie większe, niż podana liczba. Przykładowo, dla liczby 71 program powinien wyświetlić:
//        1 2 4 8 16 32 64

public class Test17 {
    public static void main(String[] args) {
        int value = 110;

        int i = 0;
        int powerVal = 0;
        System.out.println("potegi 2 mnniejsze niz " + value);
        do {
            powerVal = (int) java.lang.Math.pow(2, i++);

            if (powerVal < value)
                System.out.print(powerVal + " ");
            else
                break;
        } while (true);
        System.out.println();
    }
}
